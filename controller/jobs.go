// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package controller

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
)

type engineJob interface {
	Run(engine *engineImpl)
}

type engineAddRepoJob struct {
	Repo model.Repo
}

func (job *engineAddRepoJob) Run(engine *engineImpl) {
	log.Infof("Add repo job %s", job.Repo.String())
	repoimpl, err := engine.addRepo(job.Repo)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to add repo %s: %s",
			job.Repo.String(), err))
		return
	}

	engine.loadMergeRequestCache(repoimpl)
	engine.Listener.RepoAdded(job.Repo)

	go engine.RefreshRepository(job.Repo)
}

type engineUpdateRepoJob struct {
	Repo model.Repo
}

func (job *engineUpdateRepoJob) Run(engine *engineImpl) {
	log.Infof("Update repo job %s", job.Repo.String())
	engine.updateRepo(job.Repo)
	go engine.RefreshRepository(job.Repo)
}

type engineRemoveRepoJob struct {
	Repo model.Repo
}

func (job *engineRemoveRepoJob) Run(engine *engineImpl) {
	log.Infof("Remove repo job %s", job.Repo.String())

	for idx, impl := range engine.Repos {
		if impl.Repo.Equal(&job.Repo) {
			engine.Repos = append(engine.Repos[0:idx], engine.Repos[idx+1:]...)
			break
		}
	}

	engine.Listener.RepoRemoved(job.Repo)
}

type engineRefreshReposJob struct {
	QueuedAt time.Time
}

func (job *engineRefreshReposJob) Run(engine *engineImpl) {
	log.Info("Refresh repos job")

	engine.RefreshTimer.Stop()
	for idx, _ := range engine.Repos {
		repo := &engine.Repos[idx]
		go engine.RefreshRepositoryAt(repo.Repo, job.QueuedAt)
	}
	engine.RefreshTimer.Reset(engine.RefreshInterval)
}

type engineRefreshRepoJob struct {
	Repo     model.Repo
	QueuedAt time.Time
}

func (job *engineRefreshRepoJob) Run(engine *engineImpl) {
	log.Infof("Refresh repo job %s", job.Repo.String())
	impl := engine.getEngineRepo(job.Repo)
	if impl == nil {
		return
	}

	log.Infof("Last refresh %v this job %v", impl.RefreshedAt, job.QueuedAt)
	if impl.RefreshedAt.After(job.QueuedAt) {
		log.Infof("Refresh job queued at %v invalidated by refresh at %v",
			job.QueuedAt, impl.RefreshedAt)
		return
	}
	engine.refreshMergeRequests(impl)
}

type engineRefreshMergeRequestJob struct {
	MergeReq model.MergeReq
}

func (job *engineRefreshMergeRequestJob) Run(engine *engineImpl) {
	log.Infof("Refresh merge req job %s", job.MergeReq.String())
	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Fetching merge request %d from source",
		job.MergeReq.ID))
	newmreq, err := impl.Source.GetMergeRequest(&job.MergeReq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to refresh merge request: %s", err))
		return
	}
	newmreq.Versions = job.MergeReq.Versions
	newmreq.Threads = job.MergeReq.Threads
	engine.refreshMergeRequest(impl, &newmreq)
	newmreq.Metadata.Status = model.STATUS_UPDATED

	engine.replaceMergeRequest(impl, newmreq)
}

type engineLoadMergeRequestSeriesPatchesJob struct {
	MergeReq model.MergeReq
	Series   model.Series
}

func (job *engineLoadMergeRequestSeriesPatchesJob) Run(engine *engineImpl) {
	log.Info("Load series patches job")

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Loading %s patches %d %d",
		job.MergeReq.String(), job.Series.Index, job.Series.Version))
	patches, err := impl.Source.GetPatches(&job.MergeReq, &job.Series)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to load commit diffs: %s", err))
	} else {
		newmreq, ok := engine.cloneMergeRequest(impl, job.MergeReq.ID)
		if !ok {
			return
		}
		for sidx, _ := range newmreq.Versions {
			series := &newmreq.Versions[sidx]
			if series.Index == job.Series.Index {
				series.Patches = patches
				series.Metadata.Partial = false
			}
		}

		engine.replaceMergeRequest(impl, newmreq)
		log.Infof("Loaded series %s patches %d %d", newmreq.String(), job.Series.Index, job.Series.Version)
	}
}

type engineLoadMergeRequestCommitDiffsJob struct {
	MergeReq model.MergeReq
	Commit   model.Commit
}

func (job *engineLoadMergeRequestCommitDiffsJob) Run(engine *engineImpl) {
	log.Info("Load commit diffs job")

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Loading %s diff %s",
		job.MergeReq.String(), job.Commit.Hash))
	diffs, err := impl.Source.GetCommitDiffs(&job.MergeReq, &job.Commit)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to load commit diffs: %s", err))
	} else {
		newmreq, ok := engine.cloneMergeRequest(impl, job.MergeReq.ID)
		if !ok {
			return
		}
		for sidx, _ := range newmreq.Versions {
			series := &newmreq.Versions[sidx]
			for cidx, _ := range series.Patches {
				commit := &series.Patches[cidx]
				if commit.Hash == job.Commit.Hash {
					commit.Diffs = diffs
					commit.Metadata.Partial = false
				}
			}
		}

		engine.replaceMergeRequest(impl, newmreq)
		log.Infof("Loaded diff %s diff %s", newmreq.String(), job.Commit.Hash)
	}
}

type engineAddMergeRequestThreadJob struct {
	MergeReq   model.MergeReq
	Text       string
	Standalone bool
	Context    *model.CommentContext
}

func (job *engineAddMergeRequestThreadJob) Run(engine *engineImpl) {
	log.Infof("Add merge req thread job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	newmreq, ok := engine.cloneMergeRequest(impl, job.MergeReq.ID)
	if !ok {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Posting thread to %s", job.MergeReq.String()))
	thread, err := impl.Source.AddMergeRequestThread(&job.MergeReq, job.Text, job.Standalone, job.Context)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to post thread: %s", err))
		return
	}

	newmreq.Threads = append(newmreq.Threads, thread)

	engine.replaceMergeRequest(impl, newmreq)
}

type engineAddMergeRequestReplyJob struct {
	MergeReq model.MergeReq
	Thread   string
	Text     string
}

func (job *engineAddMergeRequestReplyJob) Run(engine *engineImpl) {
	log.Infof("Add merge req reply job %s %s", job.MergeReq.String(), job.Thread)

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	newmreq, ok := engine.cloneMergeRequest(impl, job.MergeReq.ID)
	if !ok {
		return
	}

	idx, err := newmreq.ThreadIndexFromID(job.Thread)
	if err != nil {
		engine.Listener.Status(err.Error())
		return
	}

	engine.Listener.Status(fmt.Sprintf("Posting reply to %s thread %s",
		job.MergeReq.String(), job.Thread))
	comment, err := impl.Source.AddMergeRequestReply(&job.MergeReq, job.Thread, job.Text)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to post reply: %s", err))
		return
	}

	newmreq.Threads[idx].Comments = append(newmreq.Threads[idx].Comments, comment)

	engine.replaceMergeRequest(impl, newmreq)
}

type engineResolveMergeRequestThreadJob struct {
	MergeReq model.MergeReq
	Thread   string
	Resolved bool
}

func (job *engineResolveMergeRequestThreadJob) Run(engine *engineImpl) {
	log.Infof("Resolve merge req thread job %s %s", job.MergeReq.String(), job.Thread)

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	newmreq, ok := engine.cloneMergeRequest(impl, job.MergeReq.ID)
	if !ok {
		return
	}

	idx, err := newmreq.ThreadIndexFromID(job.Thread)
	if err != nil {
		engine.Listener.Status(err.Error())
		return
	}

	if job.Resolved {
		engine.Listener.Status(fmt.Sprintf("Resolving %s thread %s",
			job.MergeReq.String(), job.Thread))
	} else {
		engine.Listener.Status(fmt.Sprintf("Unresolving %s thread %s",
			job.MergeReq.String(), job.Thread))
	}
	err = impl.Source.ResolveMergeRequestThread(&job.MergeReq, job.Thread, job.Resolved)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to resolve thread: %s", err))
		return
	}

	for cidx, _ := range newmreq.Threads[idx].Comments {
		newmreq.Threads[idx].Comments[cidx].Resolved = job.Resolved
	}
	engine.replaceMergeRequest(impl, newmreq)
}

type engineMergeRequestMarkReadJob struct {
	MergeReq model.MergeReq
}

func (job *engineMergeRequestMarkReadJob) Run(engine *engineImpl) {
	log.Infof("Mark read job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	newmreq, ok := engine.cloneMergeRequest(impl, job.MergeReq.ID)
	if !ok {
		return
	}
	newmreq.Metadata.Status = model.STATUS_READ
	log.Infof("Mark read %s", newmreq.String())
	engine.replaceMergeRequest(impl, newmreq)
}

type engineMergeRequestAcceptJob struct {
	MergeReq model.MergeReq
}

func (job *engineMergeRequestAcceptJob) Run(engine *engineImpl) {
	log.Infof("Merge request accept job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	err := impl.Source.AcceptMergeRequest(&job.MergeReq, true)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot accept merge request: %s", err))
	} else {
		engine.RefreshMergeRequest(job.MergeReq)
		engine.Listener.Status(fmt.Sprintf("Accepted merge request %s", job.MergeReq.String()))
	}
}

type engineMergeRequestApproveJob struct {
	MergeReq model.MergeReq
}

func (job *engineMergeRequestApproveJob) Run(engine *engineImpl) {
	log.Infof("Merge request approve job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	err := impl.Source.ApproveMergeRequest(&job.MergeReq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot approve merge request: %s", err))
	} else {
		engine.RefreshMergeRequest(job.MergeReq)
		engine.Listener.Status(fmt.Sprintf("Approved merge request %s", job.MergeReq.String()))
	}
}

type engineMergeRequestUnapproveJob struct {
	MergeReq model.MergeReq
}

func (job *engineMergeRequestUnapproveJob) Run(engine *engineImpl) {
	log.Infof("Merge request unapprove job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	err := impl.Source.UnapproveMergeRequest(&job.MergeReq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot unapprove merge request: %s", err))
	} else {
		engine.RefreshMergeRequest(job.MergeReq)
		engine.Listener.Status(fmt.Sprintf("Unapproved merge request %s", job.MergeReq.String()))
	}
}
