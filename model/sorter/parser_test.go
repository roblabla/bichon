// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package sorter

import (
	"testing"
)

func TestParse(t *testing.T) {
	sorters := []string{
		"age",
		"!age",
		"project, id",
		"project, ! id",
		"project, age, activity, realname, username, title, id",
	}

	for _, sorter := range sorters {
		expr, err := NewExpression(sorter)
		if err != nil {
			t.Fatalf("cannot parse '%s': %s", sorter, err)
		}

		_ = expr.BuildSorter()
	}
}
