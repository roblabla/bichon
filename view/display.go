// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"crypto/tls"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"sort"
	"strings"

	"github.com/gdamore/tcell/v2"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/config"
	"gitlab.com/bichon-project/bichon/controller"
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/model/filter"
	"gitlab.com/bichon-project/bichon/model/sorter"
	"gitlab.com/bichon-project/bichon/security"
	"gitlab.com/bichon-project/bichon/view/distractions/pacman"
	"gitlab.com/bichon-project/bichon/view/distractions/snake"
	"gitlab.com/bichon-project/tview"
)

type Display struct {
	*tview.Application

	TokenKey [32]byte
	Config   *config.AppConfig
	Keymap   *Keymap

	Engine    controller.Engine
	MergeReqs MergeReqListBatch
	Repos     []model.Repo
	Users     []string

	Layout       *tview.Flex
	KeyShortcuts *tview.TextView
	StatusBar    *tview.TextView
	MessageBar   *MessageBar
	Pages        *tview.Pages

	IndexPage    *IndexPage
	OverviewPage *OverviewPage
	DetailPage   *DetailPage
	ProjectsPage *ProjectsPage
	ReportsPage  *ReportsPage
	MessagesPage *MessagesPage
	ThisPage     Page

	Snake  *snake.Game
	Pacman *pacman.Game

	FormActive         bool
	AddProjectForm     *AddProjectForm
	EditProjectForm    *EditProjectForm
	ChoosePasswordForm *ChoosePasswordForm
	PasswordForm       *PasswordForm
	SortForm           *SortForm
	FilterForm         *FilterForm
	QuickFilterForm    *QuickFilterForm
	VersionForm        *VersionForm
	ExitConfirmForm    *ConfirmForm
	ReportForm         *ReportForm
	RunCommandForm     *RunCommandForm

	CtrlC bool

	Filter      filter.FilterInfo
	Sorter      sorter.SorterInfo
	ReportsList model.Reports
}

func NewDisplay(tlscfg *tls.Config, httpproxy *url.URL) (*Display, error) {
	err := LoadStyleConfig()
	if err != nil {
		return nil, err
	}

	reports, err := config.LoadReports()
	if err != nil {
		return nil, err
	}

	tview.TabSize = 8
	tview.Styles.PrimitiveBackgroundColor = GetStyleColor(ELEMENT_PRIMITIVE_FILL)
	tview.Styles.ContrastBackgroundColor = GetStyleColor(ELEMENT_CONTRAST_FILL)
	tview.Styles.MoreContrastBackgroundColor = GetStyleColor(ELEMENT_MORE_CONTRAST_FILL)
	tview.Styles.PrimaryTextColor = GetStyleColor(ELEMENT_PRIMARY_TEXT)
	tview.Styles.SecondaryTextColor = GetStyleColor(ELEMENT_SECONDARY_TEXT)
	tview.Styles.TertiaryTextColor = GetStyleColor(ELEMENT_TERTIARY_TEXT)
	tview.Styles.InverseTextColor = GetStyleColor(ELEMENT_INVERSE_TEXT)
	tview.Styles.ContrastSecondaryTextColor = GetStyleColor(ELEMENT_CONTRAST_SECONDARY_TEXT)
	tview.Styles.BorderColor = GetStyleColor(ELEMENT_BORDER)
	tview.Styles.TitleColor = GetStyleColor(ELEMENT_TITLE)
	tview.Styles.GraphicsColor = GetStyleColor(ELEMENT_GRAPHICS)
	tview.DefaultSelectKeys = []tcell.Key{tcell.KeyF10}

	cfg, err := config.LoadAppConfig()
	if err != nil {
		return nil, err
	}

	app := tview.NewApplication()
	msgbar := NewMessageBar(app)
	display := &Display{
		Application:  app,
		Config:       cfg,
		Keymap:       NewKeymap(),
		Layout:       tview.NewFlex(),
		KeyShortcuts: tview.NewTextView().SetDynamicColors(true),
		StatusBar:    tview.NewTextView().SetDynamicColors(true),
		MessageBar:   msgbar,
		Pages:        tview.NewPages(),
		ReportsList:  reports,
	}

	display.MergeReqs = NewMergeReqListBatch(display.mergeRequestRefresh)

	display.Engine = controller.NewEngine(display, tlscfg, httpproxy)

	display.IndexPage = NewIndexPage(display.Application, display)
	display.OverviewPage = NewOverviewPage(display.Application, display)
	display.DetailPage = NewDetailPage(display.Application, display)
	display.ProjectsPage = NewProjectsPage(display.Application, display)
	display.ReportsPage = NewReportsPage(display.Application, display)
	display.MessagesPage = NewMessagesPage(display.Application, display)

	display.AddProjectForm = NewAddProjectForm(display.Application, display)
	display.EditProjectForm = NewEditProjectForm(display.Application, display)
	display.ChoosePasswordForm = NewChoosePasswordForm(display)
	display.PasswordForm = NewPasswordForm(display)
	display.SortForm = NewSortForm(display)
	display.FilterForm = NewFilterForm(display)
	display.QuickFilterForm = NewQuickFilterForm(display)
	display.VersionForm = NewVersionForm(display)
	display.ExitConfirmForm = NewConfirmForm("exit", "Are you sure you wish to exit Bichon?", display)
	display.ReportForm = NewReportForm(display)
	display.RunCommandForm = NewRunCommandForm(display)

	display.Snake = snake.NewGame(display.Application, display)
	display.Pacman = pacman.NewGame(display.Application, display)

	err = display.Keymap.LoadConfig()
	needSave := false
	if err != nil && !os.IsNotExist(err) {
		return nil, err
	}

	var newentry bool
	pages := []Page{
		display.IndexPage,
		display.OverviewPage,
		display.DetailPage,
		display.ReportsPage,
		display.ProjectsPage,
		display.MessagesPage,
	}
	for _, page := range pages {
		newentry, err = display.Keymap.UpdateActionMap(page)
		if err != nil {
			return nil, err
		}
		if newentry {
			needSave = true
		}
	}

	if needSave {
		err = display.Keymap.SaveConfig()
		if err != nil {
			return nil, err
		}
	}

	display.SetRoot(display.Layout, true)

	display.Layout.SetDirection(tview.FlexRow).
		AddItem(display.KeyShortcuts, 1, 1, false).
		AddItem(display.Pages, 0, 1, false).
		AddItem(display.StatusBar, 1, 1, false).
		AddItem(display.MessageBar.Text, 1, 1, false)

	display.registerPage(display.IndexPage)
	display.registerPage(display.OverviewPage)
	display.registerPage(display.DetailPage)
	display.registerPage(display.ProjectsPage)
	display.registerPage(display.ReportsPage)
	display.registerPage(display.MessagesPage)
	display.registerForm(display.AddProjectForm)
	display.registerForm(display.EditProjectForm)
	display.registerForm(display.ChoosePasswordForm)
	display.registerForm(display.PasswordForm)
	display.registerForm(display.SortForm)
	display.registerForm(display.FilterForm)
	display.registerForm(display.QuickFilterForm)
	display.registerForm(display.VersionForm)
	display.registerForm(display.ExitConfirmForm)
	display.registerForm(display.ReportForm)
	display.registerForm(display.RunCommandForm)
	display.Pages.AddPage("snake", display.Snake, true, true)
	display.Pages.AddPage("pacman", display.Pacman, true, true)

	for idx, _ := range display.ReportsList {
		report := &display.ReportsList[idx]
		if report.Default {
			display.refreshFilter(filter.BuildFilter(report.Filter))
			display.refreshSorter(sorter.BuildSorter(report.Sorter))
			report.Active = true
			break
		}
	}

	display.StatusBar.SetText(fmt.Sprintf("[%s:%s]--- Bichon",
		GetStyleColorName(ELEMENT_STATUS_TEXT),
		GetStyleColorName(ELEMENT_STATUS_FILL)) +
		strings.Repeat(" ", 500))

	display.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if display.FormActive {
			return event
		}
		switch event.Key() {
		case tcell.KeyF1:
			display.Pages.SendToFront("snake")
			display.Snake.Play()
			display.Application.SetFocus(display.Snake)
			display.FormActive = true
			return nil

		case tcell.KeyF2:
			display.Pages.SendToFront("pacman")
			display.Pacman.Play()
			display.Application.SetFocus(display.Pacman)
			display.FormActive = true
			return nil
		case tcell.KeyCtrlC:
			if display.CtrlC || display.Config.Interface.DontConfirmExit {
				display.Stop()
			} else {
				display.showForm(display.ExitConfirmForm, true)
				display.CtrlC = true
				return nil
			}
		}
		return display.ThisPage.HandleInput(event)
	})

	display.switchToPage(display.IndexPage)
	display.ReportsPage.Refresh(display.ReportsList)

	if cfg.Projects.TokenProtectionKey == config.KeyBackendUndefined {
		if security.KeyringIsAvailable() {
			cfg.Projects.TokenProtectionKey = config.KeyBackendKeyring
		} else {
			cfg.Projects.TokenProtectionKey = config.KeyBackendArgon2
		}
		err = config.SaveAppConfig(cfg)
		if err != nil {
			log.Infof("Failed to save application config: %s", err)
			return nil, err
		}
	}
	if cfg.Projects.TokenProtectionKey == config.KeyBackendArgon2 {
		log.Info("Keyring is not available, falling back to user password")
		if cfg.Projects.Argon2Params == nil {
			display.showForm(display.ChoosePasswordForm, false)
		} else {
			display.showForm(display.PasswordForm, false)
		}
	} else {
		key, newKey, err := security.EnsureProtectionKey()
		if err != nil {
			return nil, err
		}

		display.TokenKey = key

		err = display.loadProjects(newKey)
		if err != nil {
			return nil, err
		}
	}

	return display, nil
}

func (display *Display) registerPage(page Page) {
	display.Pages.AddPage(page.GetName(), page, true, true)
}

func (display *Display) registerForm(form Form) {
	display.Pages.AddPage(form.GetName(), form, true, false)
}

func (display *Display) loadProjects(compatToken bool) error {
	var err error
	display.Repos, err = config.LoadProjects(display.TokenKey, compatToken)
	if err != nil {
		log.Infof("Failed to load projects: %s", err)
		return err
	}

	if compatToken {
		err = config.SaveProjects(display.Repos, display.TokenKey)
		if err != nil {
			return err
		}
	}

	for _, repo := range display.Repos {
		if repo.State == model.RepoStateActive {
			display.Engine.AddRepository(repo)
		}
	}

	log.Infof("Refreshing views")
	display.ProjectsPage.Refresh(display.Repos)
	display.FilterForm.Refresh(display.Repos, display.Users)

	if len(display.Repos) == 0 {
		log.Infof("No repos, add new projet")
		display.showForm(display.AddProjectForm, true)
		display.AddProjectForm.LoadLocalProject()
	}

	return nil
}

func (display *Display) refreshFilter(info filter.FilterInfo) {
	display.Filter = info
	display.MergeReqs.Filter = info.Matcher
	display.MergeReqs.Refresh()
	display.IndexPage.Refresh(display.MergeReqs.Active)
	display.QuickFilterForm.SetMergeReqFilter(info.Expression)
}

func (display *Display) refreshSorter(info sorter.SorterInfo) {
	display.Sorter = info
	display.MergeReqs.Sorter = info.Comparator
	display.MergeReqs.Refresh()
	display.IndexPage.Refresh(display.MergeReqs.Active)
}

func (display *Display) showForm(form Form, hasEsc bool) {
	display.FormActive = true
	display.Pages.SendToFront(form.GetName())
	display.Pages.ShowPage(form.GetName())
	display.Application.SetFocus(form)
	if hasEsc {
		display.setShortcuts("F10:Confirm Esc:Cancel")
	} else {
		display.setShortcuts("F10:Confirm")
	}
}

func (display *Display) hideForm(form Form) {
	display.Pages.SendToBack(form.GetName())
	display.Pages.HidePage(form.GetName())
	display.Application.SetFocus(display.ThisPage)
	display.setShortcuts(display.ThisPage.GetKeyShortcuts())
	display.FormActive = false
}

func (display *Display) ChoosePasswordFormConfirm(password string) {
	key, params, err := security.GenerateProtectionKeyArgon2(password)
	if err != nil {
		log.Infof("Failed to generate token protection key: %s", err)
		return
	}

	display.Config.Projects.Argon2Params = params

	err = config.SaveAppConfig(display.Config)
	if err != nil {
		log.Infof("Failed to save application config: %s", err)
		return
	}

	display.TokenKey = key

	display.hideForm(display.ChoosePasswordForm)
	display.loadProjects(false)
}

func (display *Display) PasswordFormConfirm(password string) error {
	key, err := security.ValidateProtectionKeyArgon2(password, display.Config.Projects.Argon2Params)
	if err != nil {
		log.Infof("Failed to validate token protection key: %s", err)
		return err
	}

	display.TokenKey = key

	display.hideForm(display.PasswordForm)
	display.loadProjects(false)
	return nil
}

func (display *Display) clearActiveReport() {
	for idx, _ := range display.ReportsList {
		display.ReportsList[idx].Active = false
	}
	display.ReportsPage.Refresh(display.ReportsList)
}

func (display *Display) SortFormConfirm(info sorter.SorterInfo) {
	display.refreshSorter(info)

	display.clearActiveReport()
	display.SortFormCancel()
}

func (display *Display) SortFormCancel() {
	display.hideForm(display.SortForm)
}

func (display *Display) IndexPagePickSort() {
	log.Info("Show sort")
	display.showForm(display.SortForm, true)
}

func (display *Display) FilterFormConfirm(info filter.FilterInfo) {
	display.refreshFilter(info)

	display.clearActiveReport()
	display.FilterFormCancel()
}

func (display *Display) FilterFormCancel() {
	display.hideForm(display.FilterForm)
	display.hideForm(display.QuickFilterForm)
}

func (display *Display) IndexPagePickFilter() {
	log.Info("Show filter")
	display.showForm(display.FilterForm, true)
}

func (display *Display) IndexPagePickQuickFilter() {
	log.Info("Show quick filter")
	display.showForm(display.QuickFilterForm, true)
}

func (display *Display) ConfirmFormResult(confirmed, dontaskagain bool) {
	if dontaskagain {
		display.Config.Interface.DontConfirmExit = true
		config.SaveAppConfig(display.Config)
	}
	if confirmed {
		display.Stop()
	} else {
		display.hideForm(display.ExitConfirmForm)
		display.CtrlC = false
	}
}

func (display *Display) IndexPageQuit() {
	if display.Config.Interface.DontConfirmExit {
		display.Stop()
	} else {
		display.showForm(display.ExitConfirmForm, true)
	}
}

func (display *Display) IndexPageViewMergeRequest(mreq model.MergeReq) {
	display.switchToPage(display.OverviewPage)
	display.VersionForm.Refresh(&mreq)
	display.OverviewPage.Refresh(&mreq)
	display.DetailPage.Refresh(&mreq)
	display.Engine.MarkRead(mreq)
}

func (display *Display) IndexPageViewProjects() {
	display.switchToPage(display.ProjectsPage)
}

func (display *Display) IndexPageViewReports() {
	display.switchToPage(display.ReportsPage)
}

func (display *Display) IndexPageViewMessages() {
	display.switchToPage(display.MessagesPage)
}

func (display *Display) IndexPageRefreshMergeRequests() {
	display.Engine.RefreshRepos()
}

func (display *Display) IndexPageRunCommand(mreq *model.MergeReq, ver *model.Series) {
	display.RunCommandForm.SetContext(mreq, ver, nil)
	display.showForm(display.RunCommandForm, true)
}

func (display *Display) OverviewPageQuit() {
	display.switchToPage(display.IndexPage)
}

func (display *Display) OverviewPageRefreshMergeRequest(mreq *model.MergeReq) {
	display.Engine.RefreshMergeRequest(*mreq)
}

func (display *Display) OverviewPageAcceptMergeReq(mreq *model.MergeReq) {
	display.Engine.AcceptMergeRequest(*mreq)
}

func (display *Display) OverviewPageApproveMergeReq(mreq *model.MergeReq) {
	display.Engine.ApproveMergeRequest(*mreq)
}

func (display *Display) OverviewPageUnapproveMergeReq(mreq *model.MergeReq) {
	display.Engine.UnapproveMergeRequest(*mreq)
}

func (display *Display) OverviewPagePickVersion() {
	display.showForm(display.VersionForm, true)
}

func (display *Display) OverviewPageChangePatch(num int) {
	display.switchToPage(display.DetailPage)
	display.DetailPage.SelectPatch(num)
}

func (display *Display) OverviewPageRunCommand(mreq *model.MergeReq, ver *model.Series, patch *model.Commit) {
	display.RunCommandForm.SetContext(mreq, ver, patch)
	display.showForm(display.RunCommandForm, true)
}

func (display *Display) DetailPageQuit() {
	display.switchToPage(display.OverviewPage)
}

func (display *Display) DetailPageRefreshMergeRequest(mreq *model.MergeReq) {
	display.Engine.RefreshMergeRequest(*mreq)
}

func (display *Display) DetailPageAddMergeReqComment(mreq *model.MergeReq, text string, standAlone bool, context *model.CommentContext) {
	display.Engine.AddMergeRequestThread(*mreq, text, standAlone, context)
}

func (display *Display) DetailPageAddMergeReqReply(mreq *model.MergeReq, thread, text string) {
	display.Engine.AddMergeRequestReply(*mreq, thread, text)
}

func (display *Display) DetailPageResolveMergeReqThread(mreq *model.MergeReq, thread string, resolved bool) {
	display.Engine.ResolveMergeRequestThread(*mreq, thread, resolved)
}

func (display *Display) DetailPageLoadMergeReqSeriesPatches(mreq *model.MergeReq, series *model.Series) {
	display.Engine.LoadMergeRequestSeriesPatches(*mreq, *series)
}

func (display *Display) DetailPageLoadMergeReqCommitDiffs(mreq *model.MergeReq, commit *model.Commit) {
	display.Engine.LoadMergeRequestCommitDiffs(*mreq, *commit)
}

func (display *Display) DetailPageAcceptMergeReq(mreq *model.MergeReq) {
	display.Engine.AcceptMergeRequest(*mreq)
}

func (display *Display) DetailPageApproveMergeReq(mreq *model.MergeReq) {
	display.Engine.ApproveMergeRequest(*mreq)
}

func (display *Display) DetailPageUnapproveMergeReq(mreq *model.MergeReq) {
	display.Engine.UnapproveMergeRequest(*mreq)
}

func (display *Display) DetailPagePickVersion() {
	display.showForm(display.VersionForm, true)
}

func (display *Display) DetailPageChangePatch(num int) {
	display.OverviewPage.SelectPatch(num)
}

func (display *Display) DetailPageRunCommand(mreq *model.MergeReq, ver *model.Series, patch *model.Commit) {
	display.RunCommandForm.SetContext(mreq, ver, patch)
	display.showForm(display.RunCommandForm, true)
}

func (display *Display) ProjectsPageQuit() {
	display.switchToPage(display.IndexPage)
}

func (display *Display) ProjectsPageAddRepo() {
	log.Info("Showing add projects page")
	display.showForm(display.AddProjectForm, true)
}

func (display *Display) ProjectsPageEditRepo(repo model.Repo) {
	log.Info("Showing edit projects page")
	display.EditProjectForm.SetRepo(repo)
	display.showForm(display.EditProjectForm, true)
}

func (display *Display) AddProjectFormCancel() {
	display.hideForm(display.AddProjectForm)
}

func (display *Display) EditProjectFormCancel() {
	display.hideForm(display.EditProjectForm)
}

func (display *Display) AddProjectFormConfirm(repo model.Repo) {
	if repo.GlobalToken {
		for idx, _ := range display.Repos {
			thisrepo := &display.Repos[idx]
			if thisrepo.Server == repo.Server &&
				thisrepo.GlobalToken &&
				thisrepo.Token != repo.Token {
				thisrepo.Token = repo.Token
				display.Engine.UpdateRepository(*thisrepo)
			}
		}
	}

	display.Repos = append(display.Repos, repo)

	display.ProjectsPage.Refresh(display.Repos)
	display.FilterForm.Refresh(display.Repos, display.Users)
	display.Engine.AddRepository(repo)

	err := config.SaveProjects(display.Repos, display.TokenKey)
	if err != nil {
		log.Infof("Failed to save project list: %s", err)
	}

	display.switchToPage(display.IndexPage)
}

func (display *Display) EditProjectFormConfirm(repo model.Repo) {
	log.Infof("Edit project %s", repo.String())
	for idx, oldrepo := range display.Repos {
		if oldrepo.Equal(&repo) {
			if repo.State == oldrepo.State {
				if oldrepo.Token != repo.Token || oldrepo.NickName != repo.NickName {
					log.Info("Token/nickname changed, update repo")
					display.Engine.UpdateRepository(repo)
				} else {
					log.Info("No token/nickname change")
				}
			} else {
				if repo.State == model.RepoStateActive {
					log.Info("Repo toggled in to active state")
					display.Engine.AddRepository(repo)
				} else if oldrepo.State == model.RepoStateActive {
					log.Info("Repo toggled out of active state")
					display.Engine.RemoveRepository(repo)
				} else {
					log.Info("No add/remove needed")
				}
			}
			log.Infof("Edit new state %s %d", repo.State, idx)
			display.Repos[idx] = repo
		}
	}

	if repo.GlobalToken {
		for idx, _ := range display.Repos {
			otherrepo := &display.Repos[idx]
			if !otherrepo.Equal(&repo) &&
				otherrepo.Server == repo.Server &&
				otherrepo.GlobalToken &&
				otherrepo.Token != repo.Token {
				log.Infof("Update token in project %s", otherrepo.String())
				otherrepo.Token = repo.Token
				display.Engine.UpdateRepository(*otherrepo)
			}
		}
	}

	if repo.State == model.RepoStateHidden {
		display.MergeReqs.PurgeRepo(&repo)
	}
	display.IndexPage.Refresh(display.MergeReqs.Active)

	display.ProjectsPage.Refresh(display.Repos)
	display.FilterForm.Refresh(display.Repos, display.Users)

	err := config.SaveProjects(display.Repos, display.TokenKey)
	if err != nil {
		log.Infof("Failed to save project list: %s", err)
	}

	display.switchToPage(display.ProjectsPage)
}

func (display *Display) AddProjectFormAutoFillToken(server, project string) string {
	for _, repo := range display.Repos {
		if repo.Server == server && repo.GlobalToken {
			return repo.Token
		}
	}
	return ""
}

func (display *Display) ReportsPageQuit() {
	display.switchToPage(display.IndexPage)
}

func (display *Display) ReportsPageApplyReport(report model.Report) {
	display.refreshFilter(filter.BuildFilter(report.Filter))
	display.refreshSorter(sorter.BuildSorter(report.Sorter))

	for idx, _ := range display.ReportsList {
		thisreport := &display.ReportsList[idx]
		if thisreport.Name == report.Name {
			thisreport.Active = true
		} else {
			thisreport.Active = false
		}
	}
	display.ReportsPage.Refresh(display.ReportsList)

	display.switchToPage(display.IndexPage)
}

func (display *Display) ReportsPageSetDefault(report model.Report) {
	for idx, _ := range display.ReportsList {
		thisreport := &display.ReportsList[idx]
		if thisreport.ID == report.ID {
			thisreport.Default = true
		} else {
			thisreport.Default = false
		}
	}

	err := config.SaveReports(display.ReportsList)
	if err != nil {
		log.Infof("Failed to save report list: %s", err)
	}
	display.ReportsPage.Refresh(display.ReportsList)
}

func (display *Display) ReportsPageEditReport(report model.Report) {
	display.ReportForm.SetReport(report)
	display.showForm(display.ReportForm, true)
}

func (display *Display) ReportsPageDeleteReport(report model.Report) {
	var newreports model.Reports
	wasDefault := false
	for _, thisreport := range display.ReportsList {
		if thisreport.ID == report.ID {
			wasDefault = thisreport.Default
			continue
		}
		newreports = append(newreports, thisreport)
	}
	if wasDefault && len(newreports) > 0 {
		newreports[1].Default = true
	}
	if len(newreports) == 0 {
		newreports = config.DefaultReports()
	}
	display.ReportsList = newreports
	err := config.SaveReports(display.ReportsList)
	if err != nil {
		log.Infof("Failed to save report list: %s", err)
	}
	display.ReportsPage.Refresh(display.ReportsList)
}

func (display *Display) ReportsPageAddReport() {
	report := model.NewReport("My report",
		display.Filter.Expression,
		display.Sorter.Expression,
		false)
	display.ReportForm.SetReport(report)
	display.showForm(display.ReportForm, true)
}

func (display *Display) MessagesPageQuit() {
	display.switchToPage(display.IndexPage)
}

func (display *Display) ReportFormConfirm(report model.Report) {
	idx := display.ReportsList.IndexOfReport(report)
	if idx == -1 {
		display.ReportsList = append(display.ReportsList, report)
	} else {
		display.ReportsList[idx] = report
	}
	err := config.SaveReports(display.ReportsList)
	if err != nil {
		log.Infof("Failed to save report list: %s", err)
	}
	display.ReportsPage.Refresh(display.ReportsList)
	display.ReportFormCancel()
}

func (display *Display) ReportFormCancel() {
	display.hideForm(display.ReportForm)
}

func (display *Display) RunCommandFormConfirm(cmdargs, cmdenv []string) {
	log.Infof("Run command '%s'", cmdargs[0])
	for _, arg := range cmdargs[1:] {
		log.Infof("Command arg '%s'", arg)
	}
	for _, env := range cmdenv {
		log.Infof("Command env '%s'", env)
	}

	go func() {
		cmd := exec.Command(cmdargs[0], cmdargs[1:]...)
		cmd.Env = append(os.Environ(), cmdenv...)

		display.Status(fmt.Sprintf("%s running", cmdargs[0]))
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Infof("Failed to run command: %s (%s)", err, out)
			display.Status(fmt.Sprintf("%s failed: %s", cmdargs[0], err))
		} else {
			display.Status(fmt.Sprintf("%s finished", cmdargs[0]))
		}
	}()

	display.RunCommandFormCancel()
}

func (display *Display) RunCommandFormCancel() {
	display.hideForm(display.RunCommandForm)
}

func (display *Display) VersionFormConfirm(version int) {
	display.OverviewPage.SwitchVersion(version)
	display.DetailPage.SwitchVersion(version)
	display.VersionFormCancel()
}

func (display *Display) VersionFormCancel() {
	display.hideForm(display.VersionForm)
}

func (display *Display) SnakeGameFinished() {
	display.switchToPage(display.IndexPage)
}

func (display *Display) PacmanGameFinished() {
	display.switchToPage(display.IndexPage)
}

func (display *Display) Status(msg string) {
	display.Application.QueueUpdateDraw(func() {
		display.MessageBar.Info(msg)
		display.MessagesPage.Info(msg)
	})
}

func (display *Display) refreshUniqueUsers() {
	users := make(map[string]struct{})
	for _, mreq := range display.MergeReqs.Unfiltered {
		users[mreq.Submitter.RealName] = struct{}{}
	}
	display.Users = make([]string, 0, len(users))
	for name := range users {
		display.Users = append(display.Users, name)
	}
	sort.Strings(display.Users)
}

func (display *Display) MergeRequestNotify(mreq *model.MergeReq) {
	display.MergeReqs.InsertDelayed(mreq)
}

func (display *Display) mergeRequestRefresh(mreqs []*model.MergeReq) {
	go display.Application.QueueUpdateDraw(func() {
		display.MergeReqs.InsertBatch(mreqs)
		display.refreshUniqueUsers()

		// Save the current selected mreq in case
		// the notified changes result in it being
		// filtered out of the active list
		thatmreq := display.IndexPage.GetSelectedMergeRequest()

		log.Info("Updating index page")
		display.IndexPage.Refresh(display.MergeReqs.Active)

		log.Info("Updating detail page")
		var mreq *model.MergeReq
		for _, thismreq := range mreqs {
			if thatmreq != nil && thatmreq.Equal(thismreq) {
				mreq = thismreq
			}
		}
		if mreq != nil {
			display.VersionForm.Refresh(mreq)
			display.OverviewPage.Refresh(mreq)
			display.DetailPage.Refresh(mreq)
		}

		display.FilterForm.Refresh(display.Repos, display.Users)
	})
}

func (display *Display) RepoAdded(repo model.Repo) {
}

func (display *Display) RepoRemoved(repo model.Repo) {
}

func (display *Display) setShortcuts(shortcuts string) {
	formatted := fmt.Sprintf("[%s:%s]",
		GetStyleColorName(ELEMENT_SHORTCUTS_TEXT),
		GetStyleColorName(ELEMENT_SHORTCUTS_FILL)) +
		shortcuts +
		strings.Repeat(" ", 500)
	display.KeyShortcuts.SetText(formatted)
}

func (display *Display) switchToPage(page Page) {
	display.Pages.SendToFront(page.GetName())
	display.FormActive = false
	display.ThisPage = page
	display.ThisPage.Activate()
	display.setShortcuts(display.ThisPage.GetKeyShortcuts())
}

func (display *Display) Run() error {
	return display.Application.Run()
}
