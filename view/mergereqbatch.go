// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2022 Red Hat, Inc.

package view

import (
	"time"

	"gitlab.com/bichon-project/bichon/model"
)

const BATCH_TIMER = time.Millisecond * 555

type MergeReqListBatch struct {
	model.MergeReqList

	// Invoked from controller background goroutine
	Notify  func([]*model.MergeReq)
	Pending chan *model.MergeReq
}

func NewMergeReqListBatch(notify func([]*model.MergeReq)) MergeReqListBatch {
	mbatch := MergeReqListBatch{
		Notify:  notify,
		Pending: make(chan *model.MergeReq, 1000),
	}
	go mbatch.worker()
	return mbatch
}

func (mbatch *MergeReqListBatch) worker() {
	pending := []*model.MergeReq{}
	next := time.Now().Add(BATCH_TIMER)
	for {
		if len(pending) > 0 {
			now := time.Now()
			wait := next.Sub(now)
			if wait < 0 {
				wait = time.Millisecond
			}

			timer := time.NewTimer(wait)

			select {
			case <-timer.C:
				break
			case mreq := <-mbatch.Pending:
				pending = append(pending, mreq)
				timer.Stop()
			}
		} else {
			mreq := <-mbatch.Pending
			pending = append(pending, mreq)
		}

		now := time.Now()
		if now.After(next) {
			mbatch.Notify(pending)
			pending = []*model.MergeReq{}
			next = time.Now().Add(BATCH_TIMER)
		}
	}
}

// Invoked from controller background goroutine
func (mbatch *MergeReqListBatch) InsertDelayed(mreq *model.MergeReq) {
	mbatch.Pending <- mreq
}

// Invoked from tview main goroutine
func (mbatch *MergeReqListBatch) InsertBatch(mreqs []*model.MergeReq) {
	for _, mreq := range mreqs {
		mbatch.Insert(mreq)
	}
}
