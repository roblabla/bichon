// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2021 Red Hat, Inc.

package analysis

import (
	"gitlab.com/bichon-project/bichon/model"
)

type PatchReport struct {
	Message string
	Context model.CommentContext
}

func AnalysePatch(commit *model.Commit, emoji bool) []PatchReport {
	reports := []PatchReport{}

	reports = append(reports, UnicodePatchAnalyser(commit, emoji)...)

	return reports
}
